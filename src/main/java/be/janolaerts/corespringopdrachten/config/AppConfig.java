package be.janolaerts.corespringopdrachten.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
@ComponentScan("be.janolaerts.corespringopdrachten")
@PropertySource("classpath:application.properties")
@EnableAspectJAutoProxy
//@ImportResource("classpath:/housekeeping.xml")
public class AppConfig {

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource ms =
                new ResourceBundleMessageSource();
        ms.setBasename("housekeeping");
        return ms;
    }
}