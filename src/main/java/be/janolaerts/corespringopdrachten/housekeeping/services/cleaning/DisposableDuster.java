package be.janolaerts.corespringopdrachten.housekeeping.services.cleaning;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Scope(value = "prototype", proxyMode= ScopedProxyMode.INTERFACES)
@Order(4)
@Profile("bigHouse")
public class DisposableDuster implements CleaningTool {

    private Logger logger;
    private boolean used;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void doCleanJob() {

        if(used) {
            logger.info("I'm already used. Please throw me away!");
//            System.out.println("I'm already used. Please throw me away!");

        } else {
            logger.info("Wipe the dust away");
//            System.out.println("Wipe the dust away");
            used = true;
        }
    }
}