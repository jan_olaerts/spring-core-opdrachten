package be.janolaerts.corespringopdrachten.housekeeping.services.cleaning;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Scope("prototype")
@Profile("bigHouse")
public class Sponge implements CleaningTool {

    private Logger logger;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void doCleanJob() {
        logger.info("Cleaning with sponge");
//        System.out.println("Cleaning with sponge");
    }
}