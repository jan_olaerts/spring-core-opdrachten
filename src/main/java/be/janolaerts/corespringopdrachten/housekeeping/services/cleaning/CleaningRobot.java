package be.janolaerts.corespringopdrachten.housekeeping.services.cleaning;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.logging.Logger;

@Component
@Lazy
@Profile("bigHouse")
public class CleaningRobot implements CleaningService {

    private Logger logger;
    private List<CleaningTool> tools;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Autowired
    public void setCleaningTools(List<CleaningTool> tools) {
        this.tools = tools;
    }

    @Override
    public void clean() {
        logger.info("CleaningRobot cleaning the house");
//        System.out.println("CleaningRobot cleaning the house");
        tools.forEach(CleaningTool::doCleanJob);
    }

    @PostConstruct
    public void start() {
        logger.info("Starting CleaningRobot.");
//        System.out.println("Starting CleaningRobot");
    }

    @PreDestroy
    public void stop() {
        logger.info("Stopping CleaningRobot.");
//        System.out.println("Stopping CleaningRobot");
    }
}
