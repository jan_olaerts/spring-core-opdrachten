package be.janolaerts.corespringopdrachten.housekeeping.services.garden;

public interface GardeningTool {

    void doGardenJob();
}