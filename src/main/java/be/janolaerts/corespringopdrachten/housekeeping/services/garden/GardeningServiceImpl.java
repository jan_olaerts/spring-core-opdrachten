package be.janolaerts.corespringopdrachten.housekeeping.services.garden;

import be.janolaerts.corespringopdrachten.lunch.LunchEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@Component
@Scope
@Qualifier("gardeningService")
public class GardeningServiceImpl implements GardeningService {

    private Logger logger;
    private GardeningTool tool;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Autowired
    public void setGardeningTool(GardeningTool tool) {
        this.tool = tool;
    }

    @Override
    public void garden() {
        logger.info("Working in the garden.");
//        System.out.println("Working in the garden.");
        tool.doGardenJob();
    }

    @EventListener(classes=LunchEvent.class)
    public void onLunchEvent(LunchEvent e) {
        logger.info("Taking a break for lunch");
    }

    @PostConstruct
    public void start() {
        logger.info("Starting GardeningService.");
//        System.out.println("Starting GardeningService.");
    }

    @PreDestroy
    public void stop() {
        logger.info("Stopping GardeningService.");
//        System.out.println("Stopping GardeningService.");
    }
}