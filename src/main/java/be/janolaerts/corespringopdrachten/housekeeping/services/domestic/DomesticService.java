package be.janolaerts.corespringopdrachten.housekeeping.services.domestic;

public interface DomesticService {

    void runHousehold();
}