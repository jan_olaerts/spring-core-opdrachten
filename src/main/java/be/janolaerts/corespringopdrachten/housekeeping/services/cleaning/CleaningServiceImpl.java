package be.janolaerts.corespringopdrachten.housekeeping.services.cleaning;

import be.janolaerts.corespringopdrachten.lunch.LunchEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

@Component
@Scope
@Profile("smallHouse")
@Qualifier("cleaningService")
public class CleaningServiceImpl implements CleaningService {

    private Logger logger;
    private CleaningTool tool;
    private float rate;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Autowired
    public void setCleaningTool(CleaningTool tool) {
        this.tool = tool;
    }

    @Value("${rate}")
    public void setRate(float rate) {
        this.rate = rate;
    }

    @Override
    public void clean() {
        logger.info("CleaningServiceImpl cleaning the house");
//        System.out.println("CleaningServiceImpl cleaning the house");
        tool.doCleanJob();
    }

    @EventListener(classes=LunchEvent.class)
    public void onLunchEvent(LunchEvent e) {
        logger.info("Taking a break for lunch");
    }

    @PostConstruct
    public void start() {
        logger.info("Starting CleaningService.");
//        System.out.println("Starting CleaningService");
    }

    @PreDestroy
    public void stop() {
        logger.info("Stopping CleaningService.");
        logger.info("The cleaning rate was " + this.rate + " euros.");
//        System.out.println("Stopping CleaningService");
//        System.out.println("The cleaning rate was " + this.rate + " euros.");
    }
}