package be.janolaerts.corespringopdrachten.housekeeping.services.garden;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@Profile("smallHouse")
public class LawnMower implements GardeningTool {

    @Autowired
    private Logger logger;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void doGardenJob() {
//        System.out.println("Mowing the lawn");
        logger.info("Mowing the lawn");
    }
}