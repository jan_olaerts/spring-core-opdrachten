package be.janolaerts.corespringopdrachten.housekeeping.services.cleaning;

public interface CleaningService {

    void clean();
}