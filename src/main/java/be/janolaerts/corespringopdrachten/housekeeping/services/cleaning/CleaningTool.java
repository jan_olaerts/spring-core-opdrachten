package be.janolaerts.corespringopdrachten.housekeeping.services.cleaning;

public interface CleaningTool {

    void doCleanJob();
}