package be.janolaerts.corespringopdrachten.housekeeping.services.garden;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.logging.Logger;

@Component
public class HedgeTrimmerFactory {

    private Logger logger;
    private int hour = LocalTime.now().getHour();
    private int minHour;
    private int maxHour;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Value("${minHour}")
    public void setMinHour(int minHour) {
        this.minHour = minHour;
    }

    @Value("${maxHour}")
    public void setMaxHour(int maxHour) {
        this.maxHour = maxHour;
    }

    @Bean
    @Primary
    public GardeningTool hedgeTrimmer() {

        if(hour > minHour && hour < maxHour)
//            return () -> System.out.println("Trimming hedge electrically");
            return () -> logger.info("Trimming hedge electrically");

        else
//            return () -> System.out.println("Trimming hedge manually");
            return () -> logger.info("Trimming hedge manually");
    }
}