package be.janolaerts.corespringopdrachten.housekeeping;

import be.janolaerts.corespringopdrachten.aspects.MusicMaker;
import be.janolaerts.corespringopdrachten.config.AppConfig;
import be.janolaerts.corespringopdrachten.housekeeping.services.domestic.DomesticService;
import be.janolaerts.corespringopdrachten.lunch.Cook;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class HouseApp {

    public static void main(String[] args) {

        // Java Config
        try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext()) {

            context.getEnvironment().setActiveProfiles("bigHouse");
            context.register(AppConfig.class);
            context.refresh();

            context.start();
            DomesticService service = context.getBean("domesticService", DomesticService.class);
//            Music_Interface serviceWithMusic = context.getBean("domesticService", Music_Interface.class);
            Cook cook = context.getBean("cook", Cook.class);
//            service.runHousehold();
//            context.publishEvent(new LunchEvent());
//            cook.makeLunch();
            ((MusicMaker) service).makeMusic("Song from main");

//            Computer pc = context.getBean("computer", Computer.class);
//            System.out.println(pc.toString());
        }

        // XML Config
//        try(ClassPathXmlApplicationContext context = new
//                ClassPathXmlApplicationContext()) {
//
//            context.getEnvironment().setActiveProfiles("bigHouse");
//            context.setConfigLocation("housekeeping.xml");
//            context.refresh();
//
//            DomesticService service = context.getBean("domesticService", DomesticService.class);
//            service.runHousehold();
//        }
    }
}