package be.janolaerts.corespringopdrachten.lunch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class Cook {

    @Autowired
    private Logger logger;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Autowired
    private ApplicationEventPublisher publisher;

    @EventListener(classes=ContextStartedEvent.class)
    @Order(2)
    public void makeLunch() {
        logger.info("Cook making lunch");
        publisher.publishEvent(new LunchEvent());
    }
}