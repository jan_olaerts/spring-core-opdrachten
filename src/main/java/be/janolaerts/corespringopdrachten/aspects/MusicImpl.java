package be.janolaerts.corespringopdrachten.aspects;

import org.springframework.stereotype.Component;

@Component
public class MusicImpl implements MusicMaker {

    @Override
    public void makeMusic(String song) {
        System.out.println("Singing " + song);
    }
}