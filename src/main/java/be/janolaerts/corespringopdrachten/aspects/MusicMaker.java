package be.janolaerts.corespringopdrachten.aspects;

public interface MusicMaker {

    void makeMusic(String song);
}