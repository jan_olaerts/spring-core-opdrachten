package be.janolaerts.corespringopdrachten.housekeeping.services.domestic;

import be.janolaerts.corespringopdrachten.config.AppConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig(AppConfig.class)
@PropertySource("classpath:application.properties")
@Profile("smallHouse")
public class DomesticServiceTest {

    @Autowired
    private DomesticService domesticService;

    @Test
    @DirtiesContext
    public void testDoJob() {
        domesticService.runHousehold();
    }
}