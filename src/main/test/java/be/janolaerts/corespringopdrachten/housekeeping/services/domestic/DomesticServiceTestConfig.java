package be.janolaerts.corespringopdrachten.housekeeping.services.domestic;

import be.janolaerts.corespringopdrachten.config.AppConfig;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
@ComponentScan(value = "be.janolaerts.corespringopdrachten",
        excludeFilters=@Filter(type=FilterType.REGEX, pattern=".*TestConfig.*"))
@Import(AppConfig.class)
@EnableAspectJAutoProxy
public class DomesticServiceTestConfig {

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource ms =
                new ResourceBundleMessageSource();
        ms.setBasename("housekeeping");
        return ms;
    }
}
